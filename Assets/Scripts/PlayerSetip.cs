﻿using UnityEngine;
using UnityEngine.Networking;

[RequireComponent(typeof(Player))]
public class PlayerSetip : NetworkBehaviour {

	[SerializeField] //this is so the player UI elements are attached to indiv players
	GameObject playerUIPrefab;
	private GameObject playerUIInstance;



	//list of compontents you want to disable 
	[SerializeField] //lets you see it ininspector 
	public Behaviour[] componentsToDisable;

	[SerializeField]
	private int remoteLayerName = 9; 
	//string remoteLayerName = "RemotePlayer";

	Camera sceneCamera;

	void Start()
	{

		//check if on network
		if (!isLocalPlayer) {
			DisableComponents ();
			AssignRemoteLayer ();			
		} else {
			sceneCamera = Camera.main; 

			//this is so if we dont find the camera we dont get any errors. 
			if (sceneCamera != null) {
				//disables the main camera for the scene for the local player. 
				Camera.main.gameObject.SetActive (false);
			}

			//create the playerUI
			playerUIInstance = Instantiate(playerUIPrefab); 
			playerUIInstance.name = playerUIPrefab.name; //gets rid of the clone part
		}

		GetComponent<Player> ().Setup ();

	}

	//This is called when a client joins the game 
	public override void OnStartClient()
	{
		base.OnStartClient (); //calls the base functions
		//we know we will get the network id since it extends from network behaviour 
		string _netID = GetComponent<NetworkIdentity> ().netId.ToString(); 
		//in order to ensure we always get the player the script must require componenet of player
		Player _player = GetComponent<Player>();
		GameManager.RegisterPlayer (_netID, _player);
	}


	void AssignRemoteLayer()
	{
		gameObject.layer = remoteLayerName; //LayerMask.NameToLayer (remoteLayerName);
	}

	void DisableComponents()
	{
		//go through list of all the components you want to get rid of
		for (int i = 0; i < componentsToDisable.Length; i++) {
			//disable here 
			componentsToDisable [i].enabled = false; 
		}		
	}

	void OnDisable()
	{

		//in clean up we should also desotry the old player UI
		Destroy(playerUIInstance); 

		if (sceneCamera != null) {
			//reenable the camera if you are disconnecting from level 
			sceneCamera.gameObject.SetActive (true);
		}

		//deRegister players once they are killed/disconnected
		GameManager.DeRegisterPlayer(transform.name); 


	}


}
